-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 13, 2015 at 10:27 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.6.5-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `forge`
--
CREATE DATABASE IF NOT EXISTS `forge` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `forge`;

-- --------------------------------------------------------

--
-- Table structure for table `ripbird_post_page_items`
--

CREATE TABLE IF NOT EXISTS `ripbird_post_page_items` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `post_page_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_image` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ripbird_post_pages`
--

CREATE TABLE IF NOT EXISTS `ripbird_post_pages` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `page_no` int(3) NOT NULL,
  `page_title` text,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ripbird_posts`
--

CREATE TABLE IF NOT EXISTS `ripbird_posts` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ripbird' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE IF NOT EXISTS `viralnova_posts` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `viralnova_post_items` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;



