-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Feb 23, 2015 at 06:04 PM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `forge`
--
CREATE DATABASE IF NOT EXISTS `forge` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `forge`;

-- --------------------------------------------------------

--
-- Table structure for table `ripbird_posts`
--

CREATE TABLE IF NOT EXISTS `ripbird_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='Ripbird';

-- --------------------------------------------------------

--
-- Table structure for table `ripbird_post_items`
--

CREATE TABLE IF NOT EXISTS `ripbird_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `viralnova_posts`
--

CREATE TABLE IF NOT EXISTS `viralnova_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `viralnova_post_items`
--

CREATE TABLE IF NOT EXISTS `viralnova_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `caliser_posts`
--

CREATE TABLE IF NOT EXISTS `caliser_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `caliser_post_items`
--

CREATE TABLE IF NOT EXISTS `caliser_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `greenvillegazette_posts`
--

CREATE TABLE IF NOT EXISTS `greenvillegazette_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `greenvillegazette_post_items`
--

CREATE TABLE IF NOT EXISTS `greenvillegazette_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `viewmixed_posts`
--

CREATE TABLE IF NOT EXISTS `viewmixed_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `viewmixed_post_items`
--

CREATE TABLE IF NOT EXISTS `viewmixed_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `thevirallane_posts`
--

CREATE TABLE IF NOT EXISTS `thevirallane_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `thevirallane_post_items`
--

CREATE TABLE IF NOT EXISTS `thevirallane_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `credittipstoday_posts`
--

CREATE TABLE IF NOT EXISTS `credittipstoday_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `credittipstoday_post_items`
--

CREATE TABLE IF NOT EXISTS `credittipstoday_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `dunzo_posts`
--

CREATE TABLE IF NOT EXISTS `dunzo_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dunzo_post_items`
--

CREATE TABLE IF NOT EXISTS `dunzo_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `minutemennews_posts`
--

CREATE TABLE IF NOT EXISTS `minutemennews_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `minutemennews_post_items`
--

CREATE TABLE IF NOT EXISTS `minutemennews_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `celebseven_posts`
--

CREATE TABLE IF NOT EXISTS `celebseven_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `celebseven_post_items`
--

CREATE TABLE IF NOT EXISTS `celebseven_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `mmafrenzy_posts`
--

CREATE TABLE IF NOT EXISTS `mmafrenzy_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mmafrenzy_post_items`
--

CREATE TABLE IF NOT EXISTS `mmafrenzy_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `plutobrat_posts`
--

CREATE TABLE IF NOT EXISTS `plutobrat_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plutobrat_post_items`
--

CREATE TABLE IF NOT EXISTS `plutobrat_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `mydotcomrade_posts`
--

CREATE TABLE IF NOT EXISTS `mydotcomrade_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`) 
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mydotcomrade_post_items`
--

CREATE TABLE IF NOT EXISTS `mydotcomrade_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` varchar(512) DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` varchar(512) DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `bluelionsports_posts`
--

CREATE TABLE IF NOT EXISTS `bluelionsports_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bluelionsports_post_items`
--

CREATE TABLE IF NOT EXISTS `bluelionsports_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `onlygators_posts`
--

CREATE TABLE IF NOT EXISTS `onlygators_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `onlygators_post_items`
--

CREATE TABLE IF NOT EXISTS `onlygators_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `littlethings_posts`
--

CREATE TABLE IF NOT EXISTS `littlethings_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `littlethings_post_items`
--

CREATE TABLE IF NOT EXISTS `littlethings_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `wanderingpioneer_posts`
--

CREATE TABLE IF NOT EXISTS `wanderingpioneer_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wanderingpioneer_post_items`
--

CREATE TABLE IF NOT EXISTS `wanderingpioneer_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `cavsnation_posts`
--

CREATE TABLE IF NOT EXISTS `cavsnation_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cavsnation_post_items`
--

CREATE TABLE IF NOT EXISTS `cavsnation_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `thebleacherseats_posts`
--

CREATE TABLE IF NOT EXISTS `thebleacherseats_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `thebleacherseats_post_items`
--

CREATE TABLE IF NOT EXISTS `thebleacherseats_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `usherald_posts`
--

CREATE TABLE IF NOT EXISTS `usherald_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usherald_post_items`
--

CREATE TABLE IF NOT EXISTS `usherald_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `littlebudha_posts`
--

CREATE TABLE IF NOT EXISTS `littlebudha_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `littlebudha_post_items`
--

CREATE TABLE IF NOT EXISTS `littlebudha_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `thegatewaypundit_posts`
--

CREATE TABLE IF NOT EXISTS `thegatewaypundit_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `thegatewaypundit_post_items`
--

CREATE TABLE IF NOT EXISTS `thegatewaypundit_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `sportfluff_posts`
--

CREATE TABLE IF NOT EXISTS `sportfluff_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sportfluff_post_items`
--

CREATE TABLE IF NOT EXISTS `sportfluff_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



--
-- Table structure for table `collegefootballscoop_posts`
--

CREATE TABLE IF NOT EXISTS `collegefootballscoop_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `collegefootballscoop_post_items`
--

CREATE TABLE IF NOT EXISTS `collegefootballscoop_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `movieseum_posts`
--

CREATE TABLE IF NOT EXISTS `movieseum_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `movieseum_post_items`
--

CREATE TABLE IF NOT EXISTS `movieseum_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Table structure for table `patriotvideos_posts`
--

CREATE TABLE IF NOT EXISTS `patriotvideos_posts` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `post_title` text,
  `post_desc` text,
  `is_scraped` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patriotvideos_post_items`
--

CREATE TABLE IF NOT EXISTS `patriotvideos_post_items` (
`_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `item_heading` text DEFAULT NULL,
  `item_link` varchar(256) DEFAULT NULL,
  `item_caption` text DEFAULT NULL,
  `item_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=image, 1=video',
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
