<?php
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	include 'vendor/autoload.php';
    use JonnyW\PhantomJs\Client;
    
    $client = Client::getInstance();
    
    $request  = $client->getMessageFactory()->createCaptureRequest('http://www.github.com');
    $response = $client->getMessageFactory()->createResponse();
    
    $file = 'abcd.jpg';
    
    $request->setCaptureFile($file);
    
    $client->send($request, $response);

    /*if($response->getStatus() === 200) {

        // Dump the requested page content
        
    }
    echo $response->getStatus();
    echo $response->getContent();*/