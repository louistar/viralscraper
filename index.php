<?php
/**
 * Created by PhpStorm.
 * User: Eric Louis
 * Date: 2/13/15
 * Time: 7:56 PM
 */
//header('Content-Type: application/json');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

include 'vendor/autoload.php';
use \Slim\Slim as Slim;

$app = new Slim();

$app->get('/test', function () {
    echo "abcdefhg";
});

$app->get('/hello/:id', function ($id) {
    $sql = "select * FROM temp where _id=:id";
        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("id", $id);
            $stmt->execute();
            $post = $stmt->fetchObject();
            $db = null;
            echo '{"success": ' . json_encode($post) . '}';
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
});

$app->post('/hello', function () {
    $request = Slim::getInstance()->request();
    $post = json_decode($request->getBody());

    $sql = "INSERT INTO temp (value) VALUES (:value)";
    try {
        $db = getConnection();
        //$post->value = iconv("UTF-8", "ISO-8859-1//IGNORE", $post->value);
        //$post->value = utf8_decode( $post->value);

        $stmt = $db->prepare($sql);

        $stmt->bindParam("value", $post->value);
        $stmt->execute();
        $post->_id = $db->lastInsertId();
        $db = null;
        echo '{"success": ' . json_encode($post) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
});

$app->get('/posts/:domain', 'getPosts');
$app->get('/posts/:domain/:id',  'getPost');
//$app->post('/search/posts', 'findPostByPermalLink');

$app->post('/posts/:domain', 'addPost');
$app->put('/posts/:domain/:id', 'updatePost');
$app->delete('/posts/:domain/:id',   'deletePost');

$app->post('/post_items/:domain', 'addPostItem');
$app->put('/post_items/:domain/:id', 'updatePostItem');
$app->delete('/post_items/:domain/:id',   'deletePostItem');

$app->run();

function jsonRemoveUnicodeSequences($struct) {
   return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($struct));
}

function getPosts($domain) {
    $sql = "SELECT * FROM {$domain}_posts ORDER BY _id";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $posts = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"success": ' . json_encode($posts) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getPost($domain, $id) {
    $sql = "SELECT * FROM {$domain}_posts where _id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $post = $stmt->fetchObject();
        $db = null;
        echo '{"success": ' . json_encode($post) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function findPostByPermalink($permalink, $table_prefix = 'ripbird') {
    //$request = Slim::getInstance()->request();
    $sql = "SELECT * FROM {$table_prefix}_posts WHERE permalink=:permalink";
    //$data = json_decode($request->getBody());
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("permalink", $permalink);
        $stmt->execute();
        $post = $stmt->fetchObject();
        $db = null;
        return $post;
    } catch(PDOException $e) {
        //echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return false;
}

function findPostByLink($link, $table_prefix = 'ripbird') {
    //$request = Slim::getInstance()->request();
    $sql = "SELECT * FROM ".$table_prefix."_posts WHERE link=:link";
    //$data = json_decode($request->getBody());
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("link", $link);
        $stmt->execute();
        $post = $stmt->fetchObject();
        $db = null;
        return $post;
    } catch(PDOException $e) {
        //echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return false;
}

function addPost($domain) {
    $request = Slim::getInstance()->request();
    $post = json_decode($request->getBody());
    $already_exists = findPostByLink($post->link, $domain);
    if($already_exists===false)
    {
        $sql = "INSERT INTO {$domain}_posts (link, permalink, post_title, post_desc) VALUES (:link, :permalink, :post_title, :post_desc)";
        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->bindParam("link", $post->link);
            $stmt->bindParam("permalink", $post->permalink);
            $stmt->bindParam("post_title", $post->post_title);
            $stmt->bindParam("post_desc", $post->post_desc);
            $stmt->execute();
            $post->_id = $db->lastInsertId();
            $db = null;
            echo '{"success": ' . json_encode($post) . '}';
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}';
        }
    }else if($already_exists->is_scraped==0)
    {
        echo '{"success": ' . json_encode($already_exists) . '}';
    }
    else
        echo '{"success":false}';
}

function updatePost($domain,$id) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $post = json_decode($body);
    $sql = "UPDATE {$domain}_posts SET " . (isset($post->post_title)?"post_title=:post_title, ":"") . (isset($post->post_desc)?"post_desc=:post_desc, ":"") . " is_scraped=:is_scraped WHERE _id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        if(isset($post->post_title))
            $stmt->bindParam("post_title", $post->post_title);
        if(isset($post->post_desc))
            $stmt->bindParam("post_desc", $post->post_desc);
        $stmt->bindParam("is_scraped", $post->is_scraped);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $post->_id = $id;
        $db = null;
        echo '{"success": ' . json_encode($post). '}';;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function deletePost($domain,$id) {
    $sql = "DELETE FROM {$domain}_posts WHERE _id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo '{"success": 1}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function findPostItems($data, $table_prefix, $cnt = false) {
    $sql = "SELECT * FROM {$table_prefix}_post_items WHERE post_id=:post_id and item_link=:item_link";
    if($cnt)
        $sql = "SELECT count(_id) as cnt FROM {$table_prefix}_post_items WHERE post_id=:post_id and item_link=:item_link";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("post_id", $data->post_id);
        $stmt->bindParam("item_link", $data->item_link);
        $stmt->execute();
        $post = $stmt->fetchObject();
        $db = null;
        return $post;
    } catch(PDOException $e) {
        //echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
    return false;
}

function addPostItem($domain) {
    $request = Slim::getInstance()->request();
    $post_item = json_decode($request->getBody());
    $already_exists = findPostItems($post_item, $domain);
    if($already_exists!==false)
    {
        updatePostItem($domain, $already_exists->_id);
        return;
    }

    $sql = "INSERT INTO {$domain}_post_items (post_id, item_heading, item_link, item_caption, item_type) VALUES (:post_id, :item_heading, :item_link, :item_caption, :item_type)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("post_id", $post_item->post_id);
        $stmt->bindParam("item_heading", $post_item->item_heading);
        $stmt->bindParam("item_link", $post_item->item_link);
        $stmt->bindParam("item_caption", $post_item->item_caption);
        $stmt->bindParam("item_type", $post_item->item_type);
        $stmt->execute();
        $post_item->_id = $db->lastInsertId();
        $db = null;
        echo '{"success": ' . json_encode($post_item) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function updatePostItem($domain,$id) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $post_item = json_decode($body);
    $sql = "UPDATE {$domain}_post_items SET post_id=:post_id, item_heading=:item_heading, item_link=:item_link, item_caption=:item_caption, item_type=:item_type WHERE _id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("post_id", $post_item->post_id);
        $stmt->bindParam("item_heading", $post_item->item_heading);
        $stmt->bindParam("item_link", $post_item->item_link);
        $stmt->bindParam("item_caption", $post_item->item_caption);
        $stmt->bindParam("item_type", $post_item->item_type);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $post_item->_id = $id;
        $db = null;
        echo '{"success": ' . json_encode($post_item) . '}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function deletePostItem($domain,$id) {
    $sql = "DELETE FROM {$domain}_post_items WHERE _id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
        echo '{"success": 1}';
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getConnection() {
    //$dbhost="127.0.0.1";
    $dbhost="mellow-fog";
    //$dbport = 8889;
    $dbport = 3306;
    $dbsock = '/var/run/mysqld/mysqld.sock';
    //$dbuser="root";
    $dbuser="forge";
    //$dbpass="rootroot";
    $dbpass="R51vkNyJVaz8daIBRBDD";
    $dbname="forge";
    //$dbh = new PDO("mysql:host=$dbhost;port=$dbport;dbname=$dbname", $dbuser, $dbpass);
    $dbh = new PDO("mysql:unix_socket=$dbsock;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}