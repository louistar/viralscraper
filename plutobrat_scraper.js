/**
 * Created by Cris Wong
 * @param testFx
 * @param onReady
 * @param timeOutMillis
 */

function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor()' timeout");
                    //typeof(onReady) === "string" ? eval(onReady) : onReady();
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 250); //< repeat check every 250ms
};

function PostSettings($data){
    this.operation = "POST";
    this.encoding = "utf8";
    this.headers = {
        "Content-Type": "application/json"
    };
    this.data = JSON.stringify($data);
}

    var webPage = require('webpage');
    var system = require('system');
    var page = webPage.create();
    var links = [];
    var pagers = [];
    var requests_done = 0;
    var requests_result = {};
    var api_base_url = 'http://localhost/viralscraper/';
    //var api_base_url = 'http://viralscraper.dev:8888/';
    var feed_url = 'http://plutobrat.com/feed/';
    var domainToScrape = 'plutobrat';

    var onConsoleMessageHandler = function(msg) {
        console.log(msg);
        //system.stdout.writeLine('console: '+msg);
    };

    page.viewportSize = { width: 1024, height: 768 };

    page.clipRect = { top: 0, left: 0, width: 1024, height: 768 };

    page.onConsoleMessage = onConsoleMessageHandler;
    //page.open('http://ripbird.com/valentines-cards-for-the-nerdy-couples-out-there/1/',function (s) {
    page.open(feed_url, function (s) {
    	console.log(s);

        if(s==='success') {
            console.log('Fetching Posts List API Status: ' + status);


            var parser = new DOMParser();
            var xmlDoc = parser.parseFromString(page.content, 'text/xml');
            //var links = xmlDoc.getElementsByTagName('link');
            var xPathResult = xmlDoc.evaluate('//item', xmlDoc, null, XPathResult.ANY_TYPE, null);
            var item = xPathResult.iterateNext();

            while (item) {
                var link = item.getElementsByTagName('link').item(0).textContent;
                var permalink = item.getElementsByTagName('guid').item(0).textContent;
                console.log("Link: \""+ link + "\"");
                console.log( "Permalink: \"" + permalink + "\"");

                links.push({'link':link,'permalink':permalink});
                item = xPathResult.iterateNext();
            }

            page.close();
            console.log('Links: '+links.length);
            //links.splice(3, links.length-3);//links.splice(0, 1);

            if(links.length>0) {
                requests_done = 0;
                requests_result = {};
                waitFor(function()
                {
                    return Object.keys(requests_result).length>=links.length;
                },function(){
                    var temp_links = [];
                    links.forEach(function($_link, $index){
                        if(requests_result[$index])
                            temp_links.push($_link);
                    });
                    links.splice(0,links.length);
                    links = temp_links;
                    console.log("Links: "+links.length);
                    if(links.length>0)
                        crawlLink(1);
                    else
                        phantom.exit();
                },300000);
                PublishPosts(0);
            }else
                phantom.exit(1);
            //page.render('facebook.png');
        }else{
            phantom.exit();
        }
    });

    function clearPagers() {
        while (pagers.length > 0) {
            pagers.pop();
        }
    }

    function PublishPosts(index)
    {
        var api_page = webPage.create();
        api_page.onConsoleMessage = onConsoleMessageHandler;

        var settings = new PostSettings({'link': links[index].link, 'permalink': links[index].permalink});

        api_page.open(api_base_url+'posts/' + domainToScrape , settings, function(status) {
            console.log('Publish Post API Status: ' + status);
            var result = api_page.evaluate(function(){
                return JSON.parse(document.body.innerHTML);
            });
            requests_result[index] = typeof(result.success)!=='undefined' && result['success']!==false;
            if(requests_result[index])
            {
               links[index]._id =  result['success']['_id'];
            }
            console.log("Publish Post Result: "+requests_result[index]+", ID: "+(requests_result[index]?links[index]._id:"Already Existing Or Error"));
            api_page.close();
            if(index<(links.length-1))
                PublishPosts(index+1);
        });
    }

    function UpdatePost($post_id, $result, callback)
    {
        var api_page = webPage.create();
        api_page.onConsoleMessage = onConsoleMessageHandler;

        var settings = new PostSettings({ 'post_title': $result.title_content, 'post_desc': $result.desc_content, 'is_scraped': $result.is_scraped});
        settings.operation = 'PUT';
        api_page.open(api_base_url+'posts/' + domainToScrape + '/'+$post_id, settings, function(status) {
            console.log('Update Post API Status: ' + status);
            var result = api_page.evaluate(function(){
                return JSON.parse(document.body.innerHTML);
            });
            console.log("Update Post Result: "+result['success']['_id']);
            api_page.close();
            if(result['success']!==false)
            {
                callback($post_id, $result);
            }
        });
    }

    function PublishPostItem($post_id, $index, $result, callback)
    {
        var api_page = webPage.create();
        api_page.onConsoleMessage = onConsoleMessageHandler;

        var settings = new PostSettings({'post_id': $post_id, 'item_heading': $result.section_titles[$index], 'item_link':$result.section_links[$index], 'item_type':$result.section_types[$index], 'item_caption':$result.section_captions[$index]});

        api_page.open(api_base_url+'post_items/' + domainToScrape, settings, function(status) {
            console.log('Post Item API Status: ' + status);
            var result = api_page.evaluate(function(){
                return document.body.innerHTML;
            });
            console.log("Publish Post Item Result: "+result);
            api_page.close();
            if($index<($result.section_titles.length-1))
                PublishPostItem($post_id,$index+1,$result,callback)
            else {
                //callback($result.link_index);
                $result.is_scraped = 1;
                UpdatePost($post_id, $result, function($_post_id, $_result){
                    callback($_result.link_index);
                });
            }
        });
    }

    var nav_callback = function(_index){
        links.splice(0,1);
        if(links.length>0)
            crawlLink(_index+1);
        else
            phantom.exit();
    };

    function crawlLink(index)
    {
        var page1 = webPage.create();
        page1.onConsoleMessage = onConsoleMessageHandler;

        try {
            console.log("Link "+index+": "+links[0].link);
            page1.open(links[0].link, function (s) {
                console.log("Link "+index+" status: "+s);
                var result = {};
                if(s==='success')
                {

                    result = page1.evaluate(function(_index){
                        var _result = {};
                        var title_content = '';
                        var desc_content = '';

                        var section_titles = [];
                        var section_links = [];
                        var section_captions = [];
                        var section_types = [];

                        var article_elem = document.querySelector('article[id*="post-"]');
                        if(article_elem!=null) {
                            var article_title_elem = article_elem.querySelector('h1.entry-title');
                            var article_content_elm = article_elem.querySelector('div.entry-content');


                            if (article_title_elem != null) {
                                title_content = article_title_elem.innerText.trim();
                            }
                            if (article_content_elm != null) {
                                [].forEach.call(
                                    article_content_elm.querySelectorAll('p'),
                                    function ($p) {
                                        if ($p != null && !$p.querySelector('img'))
                                            desc_content = desc_content + $p.innerText;
                                    }
                                );
                            }


                            console.log("Link " + _index + " title: " + title_content);
                            console.log("Link " + _index + " desc: " + desc_content);

                            var section_elems = article_content_elm.querySelectorAll('p img');

                            if (section_elems.length > 0) {
                                for (var i = 0; i < section_elems.length; i++) {
                                    section_titles.push('');
                                    section_captions.push(section_elems[i].parentNode.innerText);
                                    section_links.push(section_elems[i].getAttribute('src'));
                                    section_types.push(0);
                                }
                            }
                            var min_len = section_elems.length;
                            for (var i = 0; i < min_len; i++) {
                                console.log("Link " + _index + " title " + (i + 1) + ": " + section_titles[i]);
                                console.log("Link " + _index + " link " + (i + 1) + ": " + section_links[i]);
                                console.log("Link " + _index + " type " + (i + 1) + ": " + section_types[i]);
                                console.log("Link " + _index + " caption " + (i + 1) + ": " + section_captions[i]);
                            }
                        }
                        _result.title_content = title_content;
                        _result.desc_content = desc_content;
                        _result.is_scraped = 0;
                        _result.section_titles = section_titles;
                        _result.section_links = section_links;
                        _result.section_captions = section_captions;
                        _result.section_types = section_types;
                        return _result;
                    }, index);
                    //console.log("Post "+index+" content: "+result);
                    result.link_index = index;

                    UpdatePost(links[0]._id, result, function($post_id, $result){
                        if($result.section_titles.length>0)
                            PublishPostItem($post_id, 0, $result, nav_callback);
                        else
                            nav_callback($result.link_index);
                    });
                }else
                {
                    nav_callback(index);
                }
                page1.close();

                //phantom.exit();
            });
        }catch(e){
            console.log("Error on Post "+ index +": "+ e.toString());
            phantom.exit(1);
        }
    }
