/**
 * Created by Cris Wong
 * @param testFx
 * @param onReady
 * @param timeOutMillis
 */

function waitFor(testFx, onReady, timeOutMillis) {
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor()' timeout");
                    //typeof(onReady) === "string" ? eval(onReady) : onReady();
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 250); //< repeat check every 250ms
};

function PostSettings($data){
    this.operation = "POST";
    this.encoding = "utf8";
    this.headers = {
        "Content-Type": "application/json"
    };
    this.data = JSON.stringify($data);
}

    var webPage = require('webpage');
    var system = require('system');
    var page = webPage.create();
    var links = [];
    var pagers = [];
    var requests_done = 0;
    var requests_result = {};
    var api_base_url = 'http://localhost/viralscraper/';
    //var api_base_url = 'http://viralscraper.dev:8888/';
    var feed_url = 'http://movieseum.com/feed/';
    //var feed_url = 'http://ripbird.com/wp-admin/admin-ajax.php';
    var domainToScrape = 'movieseum';

    var onConsoleMessageHandler = function(msg) {
        console.log(msg);
        //system.stdout.writeLine('console: '+msg);
    };

    page.viewportSize = { width: 1024, height: 768 };

    page.clipRect = { top: 0, left: 0, width: 1024, height: 768 };

    page.onConsoleMessage = onConsoleMessageHandler;
    //page.open('http://ripbird.com/valentines-cards-for-the-nerdy-couples-out-there/1/',function (s) {
    page.open(feed_url,'POST', 'action=td_ajax_block&td_atts={"limit":500, "posts_per_page":500}&td_column_number=1&td_current_page=1&block_type=1', function (s) {
    	console.log(s);

        if(s==='success') {
            console.log('Fetching Posts List API Status: ' + status);
            /*var posts_list = page.evaluate(function(){
                var filtered_value = document.body.innerHTML.replace(/\\&quot;/g,'');
                filtered_value = filtered_value.replace(/\\"=/g,'=');
                filtered_value = filtered_value.replace(/\\\//g,'/');
                filtered_value = filtered_value.replace(/&lt;/g,'<');
                filtered_value = filtered_value.replace(/&gt;/g,'>');
                //filtered_value = filtered_value.replace("\"",'\\\"');
                //
                var pattern_links = new RegExp('<a class="photoholder" href="([^"]+)"',"gi");
                var pattern_postids = new RegExp('postid="([\\d]+)"',"gi");
                var matches;
                var unique_links = [];
                var unique_permalinks = [];
                while (matches = pattern_links.exec( filtered_value )){
                    if(unique_links.indexOf(matches[1])==-1)
                        unique_links.push(matches[1]);
                }
                while (matches = pattern_postids.exec( filtered_value )){
                    unique_permalinks.push('http://ripbird.com/?p='+matches[1]);
                }
                return {"links": unique_links, "permalinks":unique_permalinks};
            });
            posts_list.links.forEach(function($_val, $_idx){
                links.push({'link':$_val, 'permalink':($_idx==0?'':posts_list.permalinks[$_idx-1])});
            });*/

            var parser = new DOMParser();
            var xmlDoc = parser.parseFromString(page.content, 'text/xml');
            //var links = xmlDoc.getElementsByTagName('link');
            var xPathResult = xmlDoc.evaluate('//item', xmlDoc, null, XPathResult.ANY_TYPE, null);
            var item = xPathResult.iterateNext();

            while (item) {
                var link = item.getElementsByTagName('link').item(0).textContent;
                var permalink = item.getElementsByTagName('guid').item(0).textContent;
                console.log("Link: \""+ link + "\"");
                console.log( "Permalink: \"" + permalink + "\"");

                links.push({'link':link,'permalink':permalink});
                item = xPathResult.iterateNext();
            }

            page.close();
            console.log('Links: '+links.length);
            //links.splice(4, links.length-4);//links.splice(0, 2);

            if(links.length>0) {
                requests_done = 0;
                requests_result = {};
                waitFor(function()
                {
                    return Object.keys(requests_result).length>=links.length;
                },function(){
                    var temp_links = [];
                    links.forEach(function($_link, $index){
                        if(requests_result[$index])
                            temp_links.push($_link);
                    });
                    links.splice(0,links.length);
                    links = temp_links;
                    console.log("Links: "+links.length);
                    if(links.length>0)
                        crawlLink(1, 1);
                    else
                        phantom.exit();
                },300000);
                PublishPosts(0);
            }else
                phantom.exit(1);
            //page.render('facebook.png');
        }else{
            phantom.exit();
        }
    });

    function clearPagers() {
        while (pagers.length > 0) {
            pagers.pop();
        }
    }

    function PublishPosts(index)
    {
        var api_page = webPage.create();
        api_page.onConsoleMessage = onConsoleMessageHandler;

        var settings = new PostSettings({'link': links[index].link, 'permalink': links[index].permalink});

        api_page.open(api_base_url+'posts/'+domainToScrape, settings, function(status) {
            console.log('Post API Status: ' + status);
            var result = api_page.evaluate(function(){
                return JSON.parse(document.body.innerHTML);
            });
            requests_result[index] = typeof(result.success)!=='undefined' && result['success']!==false;
            if(requests_result[index])
            {
               links[index]._id =  result['success']['_id'];
            }
            console.log("Publish Post Result: "+requests_result[index]+", ID: "+(requests_result[index]?links[index]._id:"Already Existing Or Error"));
            api_page.close();
            if(index<(links.length-1))
                PublishPosts(index+1);
        });
    }

    function UpdatePost($post_id, $result, callback)
    {
        var api_page = webPage.create();
        api_page.onConsoleMessage = onConsoleMessageHandler;
        var post_data = { 'is_scraped': $result.is_scraped };
        if($result.link_pager==1)
        {
            post_data.post_title = $result.title_content;
            post_data.post_desc = $result.desc_content;
        }
        var settings = new PostSettings(post_data);

        settings.operation = 'PUT';
        api_page.open(api_base_url+'posts/' + domainToScrape + '/'+$post_id, settings, function(status) {
            console.log('Update Post API Status: ' + status);
            var result = api_page.evaluate(function(){
                return JSON.parse(document.body.innerHTML);
            });
            api_page.close();

            try {
                console.log("Update Post Result: " + result['success']['_id']);
                if (result['success'] !== false) {
                    callback($post_id, $result);
                }
            }catch(e){
                console.log(JSON.stringify(result));
                phantom.exit(1);
            }
        });
    }

    function PublishPostItem($post_id, $index, $result, callback)
    {
        var api_page = webPage.create();
        api_page.onConsoleMessage = onConsoleMessageHandler;

        var settings = new PostSettings({'post_id': $post_id, 'item_heading': $result.section_titles[$index], 'item_link':$result.section_links[$index], 'item_type':$result.section_types[$index], 'item_caption':$result.section_captions[$index]});

        api_page.open(api_base_url+'post_items/' + domainToScrape, settings, function(status) {
            console.log('Post Item API Status: ' + status);
            var result = api_page.evaluate(function(){
                return document.body.innerHTML;
            });
            console.log("Publish Post Item Result: "+result);
            api_page.close();
            if($index<($result.section_titles.length-1))
                PublishPostItem($post_id,$index+1,$result,callback)
            else {
                if($result.next_link!='')
                    callback($result.link_index, $result.link_pager, $result.next_link);
                else
                {
                    $result.is_scraped = 1;
                    UpdatePost($post_id, $result, function ($_post_id, $_result) {
                        callback($_result.link_index, $_result.link_pager, $_result.next_link);
                    });
                }
            }
        });
    }

    var nav_callback = function(_index,_pager,_next_link){
        if(_next_link!='')
        {
            crawlLink(_index,_pager+1);
        }else{
            links.splice(0,1);
            if(links.length>0)
                crawlLink(_index+1,1);
            else
                phantom.exit();
        }
    };

    function crawlLink(index, pager)
    {
        var page1 = webPage.create();
        page1.onConsoleMessage = onConsoleMessageHandler;

        try {
            console.log("Link "+index+", Page "+pager+": "+links[0].link+pager+'/');
            page1.open(links[0].link+pager+'/', function (s) {
                console.log("Link "+index+", Page "+pager+" status: "+s);
                var result = {};
                if(s==='success')
                {

                    result = page1.evaluate(function(_index,_pager){
                        var _result = {};
                        var article_elem = document.querySelector('#post-area');
                        var article_content_elm = document.querySelector('#content-area');


                        var title_content = '';
                        var desc_content = '';
                        if(_pager==1) {
                            var title_elem = document.querySelector('#title-main h1.headline');
                            if (title_elem != null)
                                title_content = title_elem.innerText;
                            var isBeforeDesc = true;
                            [].forEach.call(
                                article_content_elm.children,
                                function ($elem) {
                                    if ($elem != null && $elem.tagName.toLowerCase()=='h2') {
                                        isBeforeDesc = false;
                                        return false;
                                    }
                                    if ($elem != null && $elem.tagName.toLowerCase()=='p' && isBeforeDesc) {
                                        desc_content = desc_content + $elem.innerText;
                                    }
                                }
                            );

                            console.log("Link " + _index + " title: " + title_content);
                            console.log("Link " + _index + " desc: " + desc_content);
                        }

                        var section_titles = [];
                        var section_links= [];
                        var section_captions = [];
                        var section_types = [];


                        var section_elem = article_content_elm.querySelector('h2');

                        if(section_elem!=null) {
                            section_titles.push(section_elem.innerText.replace(/^\d+(\.\s+)?/i,'').replace(/^#\d+/i,'#'));
                            section_links.push('');
                            section_captions.push('');
                            section_types.push(0);
                            if(article_content_elm.querySelector('div[id*="attachment_"]')!=null){
                                section_links[section_links.length-1] = article_content_elm.querySelector('div[id*="attachment_"]').querySelector('img').getAttribute('src');
                            }

                            var tmp_node = section_elem;

                            while(tmp_node!=null && (tmp_node.tagName==null || !(tmp_node.tagName.toLowerCase()=='div' && tmp_node.className=='pages-buttons')))
                            {
                                if(typeof tmp_node.tagName === 'undefined' )
                                {
                                    tmp_node = tmp_node.nextSibling;
                                    continue;
                                }
                                if(tmp_node.tagName.toLowerCase()=='p')
                                {
                                    section_captions[section_captions.length-1] = section_captions[section_captions.length-1] + tmp_node.innerText;
                                }
                                tmp_node = tmp_node.nextSibling;
                            }
                            //console.log("Link "+_index+" Page "+_pager+" title count: "+section_titles.length);
                            //console.log("Link "+_index+" Page "+_pager+" images count: "+section_links.length);
                            //console.log("Link "+_index+" Page "+_pager+" caption count: "+section_captions.length);
                        }
                        var min_len = Math.min(Math.min(section_titles.length, section_links.length), section_captions.length);
                        for (var i = 0; i < min_len; i++) {
                            console.log("Link " + _index + " Page " + _pager + " title " + (i + 1) + ": " + section_titles[i]);
                            console.log("Link " + _index + " Page " + _pager + " link " + (i + 1) + ": " + section_links[i]);
                            console.log("Link " + _index + " Page " + _pager + " caption " + (i + 1) + ": " + section_captions[i]);
                        }
                        var next_link = article_content_elm.querySelector('a._next');
                        _result.next_link = '';
                        if(next_link!=null) {
                            _result.next_link = next_link.getAttribute('href');
                            if(!/\/([\d]+)\/$/.test(_result.next_link)) {
                                _result.next_link = '';
                            }
                        }

                        _result.title_content = title_content;
                        _result.desc_content = desc_content;
                        _result.is_scraped = 0;
                        _result.section_titles = section_titles;
                        _result.section_links = section_links;
                        _result.section_captions = section_captions;
                        _result.section_types = section_types;

                        return _result;
                    }, index, pager);
                    //console.log("Page "+index+" content: "+result);
                    console.log("Next link: "+result.next_link);
                    result.link_index = index;
                    result.link_pager = pager;

                    var post_item_func = function ($post_id, $result) {
                        if ($result.section_titles.length > 0)
                            PublishPostItem($post_id, 0, $result, nav_callback);
                        else {
                            if($result.next_link=='') {
                                $result.is_scraped = 1;
                                UpdatePost($post_id, $result, function ($_post_id, $_result) {
                                    nav_callback($_result.link_index, $_result.link_pager, $_result.next_link);
                                });
                            }
                            else
                                nav_callback($result.link_index, $result.link_pager, $result.next_link);
                        }
                    };

                    if(pager==1) {
                        UpdatePost(links[0]._id, result, post_item_func);
                    }else
                        post_item_func(links[0]._id, result);
                }else
                {
                    nav_callback(index,pager,'');
                }
                page1.close();

                //phantom.exit();
            });
        }catch(e){
            console.log("Error on page "+ index +": "+ e.toString());
            phantom.exit(1);
        }
    }
